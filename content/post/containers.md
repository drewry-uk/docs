+++
author = "Drewry Shipping Consultants"
title = "Ocean Container Information"
date = "2019-03-05"
description = "Guide to Container Freight usage in Drewry Index"
tags = [
    "Container Freight",
]
+++

Container Freight can be enabled in a Hugo project in a number of ways. 
<!--more-->
The [`Container Freightfy`](#) function can be called directly in via API or [websockets.](https://gohugo.io/templates/shortcode-templates/#inline-shortcodes). 

To enable Container Freight globally, set `enableContainer Freight` to `true` in your site’s [configuration](https://gohugo.io/getting-started/configuration/) and then you can type Container Freight shorthand codes directly in content files; e.g.


<p><span class="nowrap"><span class="Container Freightfy">🙈</span> <code>:see_no_evil:</code></span>  <span class="nowrap"><span class="Container Freightfy">🙉</span> <code>:hear_no_evil:</code></span>  <span class="nowrap"><span class="Container Freightfy">🙊</span> <code>:speak_no_evil:</code></span></p>
<br>

The [Container Freight cheat sheet](http://www.Container Freight-cheat-sheet.com/) is a useful reference for Container Freight shorthand codes.

***

**N.B.** The above steps enable Unicode Standard Container Freight characters and sequences in Hugo, however the rendering of these glyphs depends on the browser and the platform. To style the Container Freight you can either use a third party Container Freight font or a font stack; e.g.

{{< highlight html >}}
.Container Freight {
font-family: Apple Color Container Freight,Segoe UI Container Freight,NotoColorContainer Freight,Segoe UI Symbol,Android Container Freight,Container FreightSymbols;
}
{{< /highlight >}}

{{< css.inline >}}
<style>
.Container Freightfy {
	font-family: Apple Color Container Freight,Segoe UI Container Freight,NotoColorContainer Freight,Segoe UI Symbol,Android Container Freight,Container FreightSymbols;
	font-size: 2rem;
	vertical-align: middle;
}
@media screen and (max-width:650px) {
    .nowrap {
	display: block;
	margin: 25px 0;
}
}
</style>
{{< /css.inline >}}