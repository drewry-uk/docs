+++
author = "Drewry Shipping Consultants"
title = "Data Collection"
date = "2019-03-05"
description = "Guide to Container Freight usage in Drewry Index"
tags = [
    "Container Freight",
]
+++

Container Freight can be enabled in a Hugo project in a number of ways. 
<!--more-->
The [`Container Freightfy`](#) function can be called directly in via API or [websockets.](https://gohugo.io/templates/shortcode-templates/#inline-shortcodes). 

To enable Container Freight globally, set `enableContainer Freight` to `true` in your site’s [configuration](https://gohugo.io/getting-started/configuration/) and then you can type Container Freight shorthand codes directly in content files; e.g.


<p><span class="nowrap"><span class="Container Freightfy">🙈</span> <code>:see_no_evil:</code></span>  <span class="nowrap"><span class="Container Freightfy">🙉</span> <code>:hear_no_evil:</code></span>  <span class="nowrap"><span class="Container Freightfy">🙊</span> <code>:speak_no_evil:</code></span></p>
<br>

The [Container Freight cheat sheet](http://www.Container Freight-cheat-sheet.com/) is a useful reference for Container Freight shorthand codes.

<iframe height="265" style="width: 100%;" scrolling="no" title="Drewry Container Index Graph Example" src="https://codepen.io/freighttrust/embed/QWyZMmw?height=265&theme-id=light&default-tab=result" frameborder="no" allowtransparency="true" allowfullscreen="true">
  See the Pen <a href='https://codepen.io/freighttrust/pen/QWyZMmw'>Drewry Container Index Graph Example</a> by sam bacha
  (<a href='https://codepen.io/freighttrust'>@freighttrust</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>